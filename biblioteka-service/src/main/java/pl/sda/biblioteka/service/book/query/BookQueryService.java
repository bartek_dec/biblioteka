package pl.sda.biblioteka.service.book.query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.sda.biblioteka.domain.entity.Book;
import pl.sda.biblioteka.hibernate.repository.BookRepository;
import pl.sda.biblioteka.service.book.exception.BookNotFoundException;

import java.util.List;

@Service
@Transactional(readOnly = true)
public class BookQueryService {

    private static final Logger LOGGER = LoggerFactory.getLogger(BookQueryService.class);

    private final BookRepository bookRepository;

    @Autowired
    public BookQueryService(BookRepository bookRepository) {
        this.bookRepository = bookRepository;
    }

    public List<Book> findAll() {
        return bookRepository.findAll();
    }

    public Book findById(Long id) {
        Book book = bookRepository.findOne(id);

        if (book == null) {
            LOGGER.debug("Book with id" + book.getId() + "not found");
            throw new BookNotFoundException();
        }
        return book;
    }

    public List<Book> findAllByTitle(String title) {
        return bookRepository.findAllByTitle(title);
    }

    public List<Book> findAllByAuthorSurname(String surname) {
        return bookRepository.findAllByAuthorSurname(surname);
    }

    public List<Book> findAllByTitleAndAuthorSurname(String title, String surname) {
        return bookRepository.findAllByTitleAndAuthorSurname(title, surname);
    }

    public List<Book> findAllByReaderId(Long id) {
        return bookRepository.findAllByReaderId(id);
    }
}
