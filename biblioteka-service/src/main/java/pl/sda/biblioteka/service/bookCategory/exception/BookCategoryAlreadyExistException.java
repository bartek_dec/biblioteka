package pl.sda.biblioteka.service.bookCategory.exception;

public class BookCategoryAlreadyExistException extends RuntimeException {

    private static final long serialVersionUID = 8174139244248939536L;
}
