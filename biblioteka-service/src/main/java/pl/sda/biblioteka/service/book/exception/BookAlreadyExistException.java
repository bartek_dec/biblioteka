package pl.sda.biblioteka.service.book.exception;

public class BookAlreadyExistException extends RuntimeException {

    private static final long serialVersionUID = -2230609399732060013L;
}
