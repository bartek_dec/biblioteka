package pl.sda.biblioteka.service.reader.exception;

public class ReaderContainsBooksException extends RuntimeException {
    private static final long serialVersionUID = 56500401428956129L;
}
