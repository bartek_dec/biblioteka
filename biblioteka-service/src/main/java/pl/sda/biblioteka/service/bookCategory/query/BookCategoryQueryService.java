package pl.sda.biblioteka.service.bookCategory.query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.sda.biblioteka.domain.entity.BookCategory;
import pl.sda.biblioteka.hibernate.repository.BookCategoryRepository;
import pl.sda.biblioteka.service.bookCategory.exception.BookCategoryNotFoundException;

import java.util.List;

@Service
@Transactional(readOnly = true)
public class BookCategoryQueryService {

    private static final Logger LOGGER = LoggerFactory.getLogger(BookCategoryQueryService.class);

    private final BookCategoryRepository bookCategoryRepository;

    @Autowired
    public BookCategoryQueryService(BookCategoryRepository bookCategoryRepository) {
        this.bookCategoryRepository = bookCategoryRepository;
    }

    public List<BookCategory> findAll() {
        return bookCategoryRepository.findAll();
    }

    public BookCategory findById(Long id) {

        BookCategory dbBookCategory = bookCategoryRepository.findOne(id);

        if (dbBookCategory == null) {
            LOGGER.debug("Book category with id " + id + " not found.");
            throw new BookCategoryNotFoundException();
        }

        return dbBookCategory;
    }
}
