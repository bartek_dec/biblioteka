package pl.sda.biblioteka.service.bookCategory.command;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.sda.biblioteka.domain.entity.BookCategory;
import pl.sda.biblioteka.hibernate.repository.BookCategoryRepository;
import pl.sda.biblioteka.service.bookCategory.exception.BookCategoryContainsBooksException;
import pl.sda.biblioteka.service.bookCategory.exception.BookCategoryNotFoundException;


@Service
@Transactional
public class BookCategoryCommandService {

    private static final Logger LOGGER = LoggerFactory.getLogger(BookCategoryCommandService.class);

    private final BookCategoryRepository bookCategoryRepository;

    @Autowired
    public BookCategoryCommandService(BookCategoryRepository bookCategoryRepository) {
        this.bookCategoryRepository = bookCategoryRepository;
    }

    public Long create(BookCategory bookCategory) {
        bookCategoryRepository.save(bookCategory);

        return bookCategory.getId();
    }

    public void update(BookCategory bookCategory) {
        BookCategory dbBookCategory = bookCategoryRepository.findOne(bookCategory.getId());

        if (dbBookCategory == null) {
            LOGGER.debug("Book category with id " + bookCategory.getId() + " not found.");
            throw new BookCategoryNotFoundException();
        }

        dbBookCategory.setCategoryIdentifier(bookCategory.getCategoryIdentifier());
        dbBookCategory.setCategory(bookCategory.getCategory());
    }

    public void delete(Long id) {
        BookCategory dbBookCategory = bookCategoryRepository.findOne(id);

        if (dbBookCategory == null) {
            LOGGER.debug("Book category with id " + id + " not found.");
            throw new BookCategoryNotFoundException();
        }

        if (dbBookCategory.getBooks().size() > 0) {
            throw new BookCategoryContainsBooksException();
        }

        bookCategoryRepository.delete(dbBookCategory);
    }
}
