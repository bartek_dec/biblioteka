package pl.sda.biblioteka.service.reader.query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.sda.biblioteka.domain.entity.Reader;
import pl.sda.biblioteka.hibernate.repository.ReaderRepository;
import pl.sda.biblioteka.service.reader.exception.ReaderNotFoundException;

import java.util.List;

@Service
@Transactional(readOnly = true)
public class ReaderQueryService {

    private static final Logger LOGGER = LoggerFactory.getLogger(ReaderQueryService.class);

    private final ReaderRepository readerRepository;

    @Autowired
    public ReaderQueryService(ReaderRepository readerRepository) {
        this.readerRepository = readerRepository;
    }

    public List<Reader> findAll() {
        return readerRepository.findAll();
    }


    public Reader findById(Long id) {

        Reader reader = readerRepository.findOne(id);

        if (reader == null) {
            LOGGER.debug("Reader with id " + id + " not found.");
           throw new ReaderNotFoundException();
        }

        return reader;
    }

    public List<Reader> findAllBySurname(String surname) {
        return readerRepository.findAllBySurname(surname);
    }
}
