package pl.sda.biblioteka.service.bookCategory.exception;

public class BookCategoryNotFoundException extends RuntimeException {

    private static final long serialVersionUID = -4944729305158157779L;
}
