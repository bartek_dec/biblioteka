package pl.sda.biblioteka.service.reader.exception;

public class ReaderNotFoundException extends RuntimeException {

    private static final long serialVersionUID = 4449299495096118643L;
}
