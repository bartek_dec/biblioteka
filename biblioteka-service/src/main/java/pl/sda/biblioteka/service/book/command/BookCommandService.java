package pl.sda.biblioteka.service.book.command;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.sda.biblioteka.domain.entity.Book;
import pl.sda.biblioteka.domain.entity.BookCategory;
import pl.sda.biblioteka.hibernate.repository.BookCategoryRepository;
import pl.sda.biblioteka.hibernate.repository.BookRepository;
import pl.sda.biblioteka.service.book.exception.BookNotFoundException;
import pl.sda.biblioteka.service.book.exception.CannotRemoveBorrowedBookException;
import pl.sda.biblioteka.service.bookCategory.exception.BookCategoryNotFoundException;


@Service
@Transactional
public class BookCommandService {

    private static final Logger LOGGER = LoggerFactory.getLogger(BookCommandService.class);

    private final BookRepository bookRepository;
    private final BookCategoryRepository bookCategoryRepository;


    @Autowired
    public BookCommandService(BookRepository bookRepository,
                              BookCategoryRepository bookCategoryRepository) {
        this.bookRepository = bookRepository;
        this.bookCategoryRepository = bookCategoryRepository;
    }

    public Long create(Book book) {

        BookCategory dbCategory = bookCategoryRepository.findOne(book.getCategory().getId());

        if (book.getCategory().getId() < 0) {
            LOGGER.debug("Book category with id" + book.getCategory().getId() + " not found");
            throw new BookCategoryNotFoundException();
        }

        book.setCategory(dbCategory);

        bookRepository.save(book);

        return book.getId();
    }


    public void delete(Long id) {
        Book dbBook = bookRepository.findOne(id);

        BookCategory dbCategory = bookCategoryRepository.findOne(dbBook.getCategory().getId());

        if (dbBook == null) {
            LOGGER.debug("Book with id" + id + "not found");
            throw new BookNotFoundException();
        }

        if (dbBook.getReader() != null) {
            throw new CannotRemoveBorrowedBookException();
        }

        dbBook.setCategory(null);

        bookRepository.delete(dbBook);
    }

    public void update(Book book) {

        Book dbBook = bookRepository.findOne(book.getId());

        BookCategory dbCategory = bookCategoryRepository.findOne(book.getCategory().getId());

        if (book == null) {
            LOGGER.debug("Book with id" + book.getId() + "not found");
            throw new BookNotFoundException();
        }

        if (book.getCategory().getId() < 0) {
            LOGGER.debug("Book category with id" + book.getCategory().getId() + "not found");
            throw new BookCategoryNotFoundException();
        }

        if (!book.getCategory().getId().equals(dbBook.getCategory().getId())) {

            BookCategory previousCategory = bookCategoryRepository.findOne(dbBook.getCategory().getId());

            previousCategory.getBooks().remove(dbBook);

        }

        dbBook.setAuthorFirstName(book.getAuthorFirstName());
        dbBook.setAuthorSurname(book.getAuthorSurname());
        dbBook.setTitle(book.getTitle());
        dbBook.setCategory(dbCategory);
    }
}
