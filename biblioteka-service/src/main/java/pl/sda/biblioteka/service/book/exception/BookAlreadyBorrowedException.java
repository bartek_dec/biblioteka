package pl.sda.biblioteka.service.book.exception;

public class BookAlreadyBorrowedException extends RuntimeException {
    private static final long serialVersionUID = 3015409698632207970L;
}
