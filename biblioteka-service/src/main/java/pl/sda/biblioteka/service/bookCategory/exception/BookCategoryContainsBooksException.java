package pl.sda.biblioteka.service.bookCategory.exception;

public class BookCategoryContainsBooksException extends RuntimeException {

    private static final long serialVersionUID = 4877575727290323175L;
}
