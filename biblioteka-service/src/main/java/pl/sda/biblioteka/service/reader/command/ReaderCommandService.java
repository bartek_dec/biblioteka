package pl.sda.biblioteka.service.reader.command;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.sda.biblioteka.domain.entity.Book;
import pl.sda.biblioteka.domain.entity.Reader;
import pl.sda.biblioteka.hibernate.repository.BookRepository;
import pl.sda.biblioteka.hibernate.repository.ReaderRepository;
import pl.sda.biblioteka.service.book.exception.BookAlreadyBorrowedException;
import pl.sda.biblioteka.service.reader.exception.ReaderContainsBooksException;
import pl.sda.biblioteka.service.reader.exception.ReaderNotFoundException;

@Service
@Transactional
public class ReaderCommandService {

    private static final Logger LOGGER = LoggerFactory.getLogger(ReaderCommandService.class);

    private final ReaderRepository readerRepository;
    private final BookRepository bookRepository;

    @Autowired
    public ReaderCommandService(ReaderRepository readerRepository,
                                BookRepository bookRepository) {
        this.readerRepository = readerRepository;
        this.bookRepository = bookRepository;
    }

    public Long create(Reader reader) {

        readerRepository.save(reader);

        return reader.getId();
    }

    public void update(Reader reader) {
        Reader dbReader = readerRepository.findOne(reader.getId());

        if (dbReader == null) {
            LOGGER.debug("Reader with id " + reader.getId() + " not found.");
            throw new ReaderNotFoundException();
        }

        dbReader.setName(reader.getName());
        dbReader.setSurname(reader.getSurname());
        dbReader.setEmail(reader.getEmail());
        dbReader.setBirthdate(reader.getBirthdate());
        dbReader.getAddress().setCity(reader.getAddress().getCity());
        dbReader.getAddress().setStreet(reader.getAddress().getStreet());
        dbReader.getAddress().setHouseNo(reader.getAddress().getHouseNo());
        dbReader.getAddress().setFlatNo(reader.getAddress().getFlatNo());
        dbReader.getAddress().setPostalCode(reader.getAddress().getPostalCode());
    }

    public void delete(Long id) {
        Reader dbReader = readerRepository.findOne(id);

        if (dbReader == null) {
            LOGGER.debug("Reader with id " + id + " not found.");
            throw new ReaderNotFoundException();
        }

        if (dbReader.getBorrowedBooks().size() > 0) {
            throw new ReaderContainsBooksException();
        }

        readerRepository.delete(dbReader);
    }

    public void borrow(Long id, Book book) {
        Book dbBook = bookRepository.findOne(book.getId());

        if (dbBook.getReader() != null) {
            throw new BookAlreadyBorrowedException();
        }

        Reader dbReader = readerRepository.findOne(id);

        dbBook.setReader(dbReader);
        dbBook.setBorrowDate(book.getBorrowDate());

    }

    public void returnBook(Long id, Long bookId) {
        Book dbBook = bookRepository.findOne(bookId);
        Reader dbReader = readerRepository.findOne(id);

        dbBook.setReader(null);
        dbBook.setBorrowDate(null);

    }
}
