package pl.sda.biblioteka.service.book.exception;

public class CannotRemoveBorrowedBookException extends RuntimeException {

    private static final long serialVersionUID = 7231196296045085504L;
}
