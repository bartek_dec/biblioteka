package pl.sda.biblioteka.domain.entity;


import org.hibernate.envers.Audited;
import org.hibernate.validator.constraints.NotBlank;
import pl.sda.biblioteka.domain.baseentity.BaseEntity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import java.util.Date;

@Entity
@Table(name = "BOOK")
@Audited
@AttributeOverride(name = "id", column = @Column(name = "ID"))
public class Book extends BaseEntity {

    private static final long serialVersionUID = -7517975487337305101L;

    @NotBlank
    @Column(name = "AUTHOR_FIRST_NAME")
    private String authorFirstName;

    @NotBlank
    @Column(name = "AUTHOR_SURNAME")
    private String authorSurname;

    @NotBlank
    @Column(name = "TITLE")
    private String title;

    @Past
    @Column(name = "BORROW_DATE")
    private Date borrowDate;

    @NotNull
    @ManyToOne(cascade = CascadeType.ALL)
    private BookCategory category;

    @ManyToOne(cascade = CascadeType.ALL)
    private Reader reader;

    public Book() {

    }

    public Book(String author, String title) {
        this.authorFirstName = author;
        this.title = title;
    }


    public String getAuthorFirstName() {
        return authorFirstName;
    }

    public void setAuthorFirstName(String authorFirstName) {
        this.authorFirstName = authorFirstName;
    }

    public String getAuthorSurname() {
        return authorSurname;
    }

    public void setAuthorSurname(String authorSurname) {
        this.authorSurname = authorSurname;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Date getBorrowDate() {
        return borrowDate;
    }

    public void setBorrowDate(Date borrowDate) {
        this.borrowDate = borrowDate;
    }

    public BookCategory getCategory() {
        return category;
    }

    public void setCategory(BookCategory category) {
        this.category = category;
    }

    public Reader getReader() {
        return reader;
    }

    public void setReader(Reader reader) {
        this.reader = reader;
    }

    @Override
    public String toString() {
        return "Book{" + "id=" + id +
                ", author first name=" + authorFirstName +
                ", author surname=" + authorSurname +
                ", title=" + title +
                ", category =" + category +
                "}";
    }
}
