package pl.sda.biblioteka.domain.entity;


import org.hibernate.envers.Audited;
import org.hibernate.validator.constraints.NotBlank;
import pl.sda.biblioteka.domain.baseentity.BaseEntity;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "BOOK_CATEGORY")
@Audited
@AttributeOverride(name = "id", column = @Column(name = "ID"))

public class BookCategory extends BaseEntity {

    private static final long serialVersionUID = -5037635597979132910L;

    @NotBlank
    @Column(name = "CATEGORY_IDENTIFIER")
    private String categoryIdentifier;

    @NotBlank
    @Column(name = "CATEGORY")
    private String category;

    @OneToMany(mappedBy = "category", cascade = CascadeType.ALL)
    private Set<Book> books = new HashSet<>();

    public BookCategory() {

    }

    public String getCategoryIdentifier() {
        return categoryIdentifier;
    }

    public void setCategoryIdentifier(String categoryIdentifier) {
        this.categoryIdentifier = categoryIdentifier;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public Set<Book> getBooks() {
        return books;
    }

    public void setBooks(Set<Book> books) {
        this.books = books;
    }

    @Override
    public String toString() {
        return category;
    }
}
