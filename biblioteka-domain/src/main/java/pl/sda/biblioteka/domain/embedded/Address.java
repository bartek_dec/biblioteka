package pl.sda.biblioteka.domain.embedded;

import org.hibernate.envers.Audited;
import org.hibernate.validator.constraints.NotBlank;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Embeddable
@Audited
public class Address {

    @NotBlank
    @Column(name = "CITY")
    private String city;

    @NotBlank
    @Column(name = "STREET")
    private String street;

    @NotNull
    @Min(1)
    @Column(name = "HOUSE_NUMBER")
    private Integer houseNo;

    @Column(name = "FLAT_NUMBER")
    private String  flatNo;

    @NotBlank
    @Column(name = "POSTAL_CODE")
    private String postalCode;

    public Address() {

    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public Integer getHouseNo() {
        return houseNo;
    }

    public void setHouseNo(Integer houseNo) {
        this.houseNo = houseNo;
    }

    public String getFlatNo() {
        return flatNo;
    }

    public void setFlatNo(String flatNo) {
        this.flatNo = flatNo;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }
}
