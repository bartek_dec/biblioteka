package pl.sda.biblioteka.domain.entity;

import org.hibernate.envers.Audited;
import org.hibernate.validator.constraints.NotBlank;
import pl.sda.biblioteka.domain.baseentity.BaseEntity;
import pl.sda.biblioteka.domain.embedded.Address;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "READER")
@Audited
@AttributeOverride(name = "id", column = @Column(name = "ID"))
public class Reader extends BaseEntity {

    private static final long serialVersionUID = -1456166877899402329L;

    @NotBlank
    @Column(name = "NAME")
    private String name;

    @NotBlank
    @Column(name = "SURNAME")
    private String surname;

    @Column(name = "EMAIL")
    private String email;

    @NotNull
    @Past
    @Column(name = "BIRTHDATE")
    private Date birthdate;

    @Valid
    @Embedded
    private Address address;


    @OneToMany(mappedBy = "reader", cascade = CascadeType.ALL)
    private Set<Book> borrowedBooks = new HashSet<>();

    public Reader() {

    }

    public Reader(String name, String surname) {
        this.name = name;
        this.surname = surname;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Date getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(Date birthdate) {
        this.birthdate = birthdate;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public Set<Book> getBorrowedBooks() {
        return borrowedBooks;
    }

    public void setBorrowedBooks(Set<Book> borrowedBooks) {
        this.borrowedBooks = borrowedBooks;
    }

    @Override
    public String toString() {
        return "Reader{" + "id=" + id +
                ", name=" + name +
                ", surname=" + surname +
                ", email=" + email +
                ", address=" + address +
                ", birthdate=" + birthdate +
                "}";
    }
}
