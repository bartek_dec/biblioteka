package pl.sda.biblioteka.app.web.controllers;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import pl.sda.biblioteka.domain.entity.BookCategory;
import pl.sda.biblioteka.service.bookCategory.command.BookCategoryCommandService;
import pl.sda.biblioteka.service.bookCategory.query.BookCategoryQueryService;

import java.util.Arrays;

import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


public class BookCategoryControllerTest {

    private MockMvc mockMvc;

    @InjectMocks
    private BookCategoryController bookCategoryController;

    @Mock
    private BookCategoryQueryService bookCategoryQueryService;

    @Mock
    private BookCategoryCommandService bookCategoryCommandService;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);

        mockMvc = MockMvcBuilders.standaloneSetup(bookCategoryController).build();
    }

    @Test
    public void testMainPageOfBookCategory() throws Exception {
        BookCategory filozofia = createCategory(1L, "FIL", "Filozofia");
        BookCategory medycyna = createCategory(2L, "MD", "Medycyna");

        when(bookCategoryQueryService.findAll()).thenReturn(Arrays.asList(filozofia, medycyna));

        mockMvc.perform(get("/bookCategory"))
                .andExpect(status().isOk())
                .andExpect(view().name("addBookCategory"))
                .andExpect(model().attribute("listBookCategories", hasSize(2)));
    }

    @Test
    public void testSaveBookCategory()throws Exception {
        mockMvc.perform(post("/bookCategory/save"))
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("/bookCategory"));
    }

    @Test
    public void testEditBookCategory() throws Exception {
        Long id = 1L;

        when(bookCategoryQueryService.findById(id)).thenReturn(new BookCategory());

        mockMvc.perform(get("/bookCategory/edit/1"))
                .andExpect(status().isOk())
                .andExpect(view().name("addBookCategory"))
                .andExpect(model().attribute("bookCategory", instanceOf(BookCategory.class)));
    }

    @Test
    public void testDeleteBookCategory() throws Exception {
        Long id = 1L;

        doNothing().when(bookCategoryCommandService).delete(id);

        mockMvc.perform(get("/bookCategory/delete/" + id))
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("/bookCategory"));

        verify(bookCategoryCommandService, times(1)).delete(id);
    }

    private BookCategory createCategory(Long id, String identifier, String category) {
        BookCategory bookCategory = new BookCategory();

        bookCategory.setId(id);
        bookCategory.setCategoryIdentifier(identifier);
        bookCategory.setCategory(category);

        return bookCategory;
    }
}