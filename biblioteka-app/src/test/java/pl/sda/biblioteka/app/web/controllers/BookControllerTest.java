package pl.sda.biblioteka.app.web.controllers;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import pl.sda.biblioteka.domain.entity.Book;
import pl.sda.biblioteka.domain.entity.BookCategory;
import pl.sda.biblioteka.service.book.command.BookCommandService;
import pl.sda.biblioteka.service.book.query.BookQueryService;
import pl.sda.biblioteka.service.bookCategory.query.BookCategoryQueryService;

import java.util.Arrays;
import java.util.Date;

import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class BookControllerTest {

    private MockMvc mockMvc;

    @InjectMocks
    private BookController bookController;

    @Mock
    private BookCommandService bookCommandService;

    @Mock
    private BookQueryService bookQueryService;

    @Mock
    private BookCategoryQueryService bookCategoryQueryService;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);

        mockMvc = MockMvcBuilders.standaloneSetup(bookController).build();
    }

    @Test
    public void testMainPageOfBook() throws Exception {
        BookCategory kategoria = new BookCategory();

        kategoria.setId(1L);
        kategoria.setCategoryIdentifier("LIT");
        kategoria.setCategory("Literatura");

        Book book1 = createBook(1L, "Adam", "Mickiewicz",
                "Dziady", new Date(), kategoria);

        Book book2 = createBook(2L, "Jack", "London",
                "Biały Kieł", new Date(), kategoria);

        when(bookCategoryQueryService.findAll()).thenReturn(Arrays.asList(kategoria));
        when(bookQueryService.findAll()).thenReturn(Arrays.asList(book1, book2));

        mockMvc.perform(get("/book"))
                .andExpect(status().isOk())
                .andExpect(view().name("addBook"))
                .andExpect(model().attribute("listBooks", hasSize(2)))
                .andExpect(model().attribute("categories", hasSize(1)));
    }

    @Test
    public void testSaveBook() throws Exception {
        mockMvc.perform(post("/book/save"))
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("/book"));
    }

    @Test
    public void testFindBookMainPage() throws Exception {
        mockMvc.perform(get("/book/find"))
                .andExpect(status().isOk())
                .andExpect(view().name("findBook"))
                .andExpect(model().attribute("book", instanceOf(Book.class)));
    }

    @Test
    public void testEditBook() throws Exception {
        Long id = 1L;

        when(bookQueryService.findById(id)).thenReturn(new Book());

        mockMvc.perform(get("/book/edit/1"))
                .andExpect(status().isOk())
                .andExpect(view().name("addBook"))
                .andExpect(model().attribute("book", instanceOf(Book.class)));
    }

    @Test
    public void testDeleteBook() throws Exception {
        Long id = 1L;

        doNothing().when(bookCommandService).delete(id);

        mockMvc.perform(get("/book/delete/" + id))
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("/book"));

        verify(bookCommandService, times(1)).delete(id);
    }

    private Book createBook(Long id, String authorFirstName,
                            String authorSurName, String title,
                            Date borrowDate, BookCategory bookCategory) {
        Book book = new Book();
        book.setId(id);
        book.setAuthorFirstName(authorFirstName);
        book.setAuthorSurname(authorSurName);
        book.setTitle(title);
        book.setBorrowDate(borrowDate);
        book.setCategory(bookCategory);

        return book;
    }
}