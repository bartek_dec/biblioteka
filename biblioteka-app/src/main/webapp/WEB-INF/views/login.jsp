<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>

<!DOCTYPE html>
<html lang="PL">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title><spring:message code="application.title"/></title>

    <!-- Bootstrap -->
    <link href="<c:url value="/resources/css/bootstrap.css" />" rel="stylesheet" type="text/css">
    <link href="<c:url value="/resources/css/fontello.css" />" rel="stylesheet" type="text/css">
    <script src="<c:url value="/resources/js/timer.js" />"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.3/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.min.css"
          rel="stylesheet"/>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<div class="container">
    <div class="row" style="height: 150px"></div>

    <div class="row">

        <form:form class="form-horizontal" action="do_login" method="post">
            <fieldset>
                <legend class="col-lg-6 col-lg-offset-4"><spring:message code="userLogin.form.title"/></legend>

                <div class="form-group">
                    <label for="inputLogin" class="col-lg-4 control-label"></label>
                    <div class="col-lg-4 ">
                        <spring:message code="userLogin.form.label.userLogin" var="userLogin"/>
                        <input class="form-control" id="inputLogin" placeholder="${userLogin}" type="text" name='userlogin'
                               value=''>
                    </div>
                </div>

                <div class="form-group">
                    <label for="inputPassword" class="col-lg-4 control-label"></label>
                    <div class="col-lg-4">
                        <spring:message code="userLogin.form.label.password" var="userPassword"/>
                        <input class="form-control" id="inputPassword" placeholder="${userPassword}" type="password"
                               name='userpassword'/>

                        <c:if test="${not empty error}">
                            <div class="text-danger">${error}</div>
                        </c:if>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-lg-6 col-lg-offset-4">
                        <spring:message code="userLogin.form.submit.label" var="labelSubmit"/>
                        <input type="submit" class="btn btn-primary" name="submit" type="submit"
                               value="${labelSubmit}"/>

                    </div>
                </div>

            </fieldset>
        </form:form>
    </div>

</div>


<div class="container">
    <div class="row">
        <div class="navbar-fixed-bottom">
            <div class="col-lg-12" align="center">
                <footer class="page-footer blue center-on-small-only">

                    <!--Copyright-->
                    <div class="footer-copyright">
                        <div class="container-fluid">
                            <spring:message code="footer.info"/>
                            <i class="icon-mail-alt"></i>
                            <spring:message code="footer.email"/>
                        </div>
                    </div>
                    <!--/.Copyright-->

                </footer>
            </div>
        </div>
    </div>
</div>

</body>
</html>
