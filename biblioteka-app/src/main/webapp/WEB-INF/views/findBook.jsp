<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>

<!DOCTYPE html>
<html lang="PL">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title><spring:message code="reader.title"/></title>

    <!-- Bootstrap -->
    <link href="<c:url value="/resources/css/bootstrap.css" />" rel="stylesheet" type="text/css">
    <link href="<c:url value="/resources/css/fontello.css" />" rel="stylesheet" type="text/css">
    <script src="<c:url value="/resources/js/timer.js" />"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.3/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.min.css"
          rel="stylesheet"/>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>
<body onload="odliczanie();">
<div class="container">

    <nav class="navbar navbar-default">
        <div class="container-fluid">
            <div class="navbar-header">
                <a class="navbar-brand" href="<c:url value="/main"/>">
                    <spring:message code="main.label"/>
                </a>
            </div>

            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">

                <ul class="nav navbar-nav">

                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                            <spring:message code="reader.title"/><span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="<c:url value="/reader"/>">
                                <spring:message code="reader.label"/>
                            </a></li>
                            <li><a href="<c:url value="/reader/find"/>">
                                <spring:message code="reader.find.label"/>
                            </a></li>
                        </ul>
                    </li>

                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                            <spring:message code="book.title"/><span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="<c:url value="/book"/>">
                                <spring:message code="book.label"/>
                            </a></li>

                            <li><a href="<c:url value="/bookCategory"/>">
                                <spring:message code="bookCategory.label"/>
                            </a></li>
                            <li><a href="<c:url value="/book/find"/>">
                                <spring:message code="book.findBook.label"/>
                            </a></li>
                        </ul>
                    </li>
                </ul>

                <ul class="nav navbar-nav">
                    <li>
                        <c:url var="logoutUrl" value="/perform_logout"/>
                        <spring:message code="logout.submit.label" var="logout"/>

                        <form:form method="post" action="${logoutUrl}">
                            <input class="navbar-header navbar-brand" style="background-color: transparent;"
                                   type="submit" value="${logout}">
                        </form:form>
                    </li>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <li id="zegar">12:00:00</li>
                </ul>

            </div>

        </div>
    </nav>

    <div class="row" style="height: 0px"></div>
</div>

<div class="container">
    <div class="row">

        <!--div z formularzem-->
        <div class="col-lg-2">
            <c:url var="saveAction" value="/book/find"/>
            <form:form modelAttribute="book" class="form-horizontal" method="post" action="${saveAction}">
                <fieldset>

                    <div class="form-group">
                        <form:label path="title" for="inputTytu" class="col-lg-0 control-label"/>
                        <div class="col-lg-12">
                            <spring:message code="book.find.label.title" var="title"/>

                            <form:input path="title" class="form-control" id="inputTytul" placeholder="${title}"
                                        type="text"/>
                        </div>
                    </div>

                    <div class="form-group">
                        <form:label path="authorSurname" for="inputTytu" class="col-lg-0 control-label"/>
                        <div class="col-lg-12">
                            <spring:message code="book.find.label.authorSurname" var="surname"/>

                            <form:input path="authorSurname" class="form-control" id="inputTytul"
                                        placeholder="${surname}"
                                        type="text"/>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-lg-2 col-lg-offset-0">
                            <spring:message code="book.find.label.submit" var="labelSubmit"/>
                            <input class="btn btn-success" name="submit" type="submit" value="${labelSubmit}"/>
                        </div>
                    </div>
                </fieldset>
            </form:form>

        </div>

        <!--div z tabelą-->
        <div class="col-lg-10">

            <c:choose>
            <c:when test="${!empty foundBooks}">
            <div class="panel panel-default">
                <table class="table table-striped table-hover ">
                    <thead>
                    <tr class="info">
                        <th class="col-md-1"><spring:message code="book.id"/></th>
                        <th class="col-md-1"><spring:message code="book.authorFirstName"/></th>
                        <th class="col-md-1"><spring:message code="book.authorSurname"/></th>
                        <th class="col-md-2"><spring:message code="book.book.title"/></th>
                        <th class="col-md-1"><spring:message code="book.book.category"/></th>
                        <th class="col-md-1"><spring:message code="book.state"/></th>
                        <th class="col-md-1"><spring:message code="book.who.borrowed"/></th>
                        <th class="col-md-1"></th>
                        <th class="col-md-1"></th>
                    </tr>
                    </thead>
                    <tbody>
                    <c:forEach items="${foundBooks}" var="book">
                        <tr>
                            <td>${book.id}</td>
                            <td>${book.authorFirstName}</td>
                            <td>${book.authorSurname}</td>
                            <td>${book.title}</td>
                            <td>${book.category.category}</td>
                            <td>
                                <c:choose>
                                    <c:when test="${empty book.reader}">
                                        <p class="text-success">
                                            <spring:message code="book.state.available"/>
                                        </p>
                                    </c:when>
                                    <c:otherwise>
                                        <p class="text-danger">
                                            <spring:message code="book.state.notavailable"/>
                                        </p>
                                    </c:otherwise>
                                </c:choose>
                            </td>
                            <td>
                                    ${book.reader.surname}
                            </td>
                            <td>
                            <a class="btn btn-warning btn-xs"
                               href="<c:url value='/book/edit/${book.id}'/>">
                                <spring:message code="book.action.edit"/>
                            </a>
                            </td>
                            <td>
                                <a class="btn btn-danger btn-xs"
                                   href="<c:url value='/book/delete/${book.id}'/>">
                                    <spring:message code="book.action.delete"/>
                                </a>
                            </td>
                        </tr>
                    </c:forEach>
                    </c:when>

                    <c:otherwise>
                        <div class="jumbotron">
                            <h2><spring:message code="book.message.empty"/></h2>
                        </div>
                    </c:otherwise>
                    </c:choose>
                    </tbody>
                </table>
            </div>


        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="navbar-fixed-bottom">
                <div class="col-lg-12" align="center">
                    <footer class="page-footer blue center-on-small-only">

                        <!--Copyright-->
                        <div class="footer-copyright">
                            <div class="container-fluid">
                                <spring:message code="footer.info"/>
                                <i class="icon-mail-alt"></i>
                                <spring:message code="footer.email"/>
                            </div>
                        </div>
                        <!--/.Copyright-->

                    </footer>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>

