package pl.sda.biblioteka.app.dto;

import javax.validation.constraints.Min;
import javax.validation.constraints.Past;
import java.io.Serializable;
import java.util.Date;

public class FoundBookDTO implements Serializable {

    private static final long serialVersionUID = -8379126717259038412L;

    @Min(1)
    private Long bookID;

    @Past
    private Date borrowDate;

    public FoundBookDTO() {

    }

    public Long getBookID() {
        return bookID;
    }

    public void setBookID(Long bookID) {
        this.bookID = bookID;
    }

    public Date getBorrowDate() {
        return borrowDate;
    }

    public void setBorrowDate(Date borrowDate) {
        this.borrowDate = borrowDate;
    }
}
