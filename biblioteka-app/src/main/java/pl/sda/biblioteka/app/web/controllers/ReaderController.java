package pl.sda.biblioteka.app.web.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import pl.sda.biblioteka.app.dto.FoundBookDTO;
import pl.sda.biblioteka.domain.entity.Book;
import pl.sda.biblioteka.domain.entity.Reader;
import pl.sda.biblioteka.service.book.exception.BookAlreadyBorrowedException;
import pl.sda.biblioteka.service.book.exception.BookNotFoundException;
import pl.sda.biblioteka.service.book.query.BookQueryService;
import pl.sda.biblioteka.service.reader.command.ReaderCommandService;
import pl.sda.biblioteka.service.reader.exception.ReaderContainsBooksException;
import pl.sda.biblioteka.service.reader.query.ReaderQueryService;

import javax.validation.Valid;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

@Controller
public class ReaderController {

    private static final Logger LOGGER = LoggerFactory.getLogger(ReaderController.class);

    private final ReaderCommandService readerCommandService;
    private final ReaderQueryService readerQueryService;
    private final BookQueryService bookQueryService;
    private final MessageSource messageSource;

    @Autowired
    public ReaderController(ReaderCommandService readerCommandService,
                            ReaderQueryService readerQueryService,
                            BookQueryService bookQueryService,
                            MessageSource messageSource) {
        this.readerCommandService = readerCommandService;
        this.readerQueryService = readerQueryService;
        this.bookQueryService = bookQueryService;
        this.messageSource = messageSource;
    }


    @InitBinder
    protected void initBinder(WebDataBinder binder) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        CustomDateEditor editor = new CustomDateEditor(dateFormat, true);
        binder.registerCustomEditor(Date.class, editor);
    }

    @RequestMapping(value = "/reader", method = RequestMethod.GET)
    public String getPage(@ModelAttribute("flashReader") Reader reader,
                          Model model) {

        LOGGER.debug("Reader getPage is executed!");

        model.addAttribute("listReaders", readerQueryService.findAll());
        model.addAttribute("reader", reader);

        return "addReader";
    }

    @RequestMapping(value = "/reader/save", method = RequestMethod.POST)
    public String saveReader(@Valid @ModelAttribute("reader") Reader reader,
                             BindingResult bindingResult, RedirectAttributes redirectAttributes) {
        LOGGER.debug("Reader saveReader is executed!");

        if (bindingResult.hasErrors()) {
            redirectAttributes.addFlashAttribute("org.springframework.validation.BindingResult.reader", bindingResult);
            redirectAttributes.addFlashAttribute("flashReader", reader);

            return "redirect:/reader";
        }

        if (reader.getId() == null) {
            readerCommandService.create(reader);
        } else {
            readerCommandService.update(reader);
        }

        return "redirect:/reader";
    }

    @RequestMapping(value = "/reader/edit/{id}")
    public String editReader(@PathVariable("id") Long id, Model model) {

        LOGGER.debug("Reader editReader is executed!");

        model.addAttribute("listReaders", readerQueryService.findAll());
        model.addAttribute("reader", readerQueryService.findById(id));

        return "addReader";
    }

    @RequestMapping(value = "/reader/delete/{id}")
    public String deleteReader(@PathVariable("id") Long id) {

        LOGGER.debug("Reader deleteReader is executed!");

        readerCommandService.delete(id);

        return "redirect:/reader";
    }


    @RequestMapping(value = "/reader/find", method = RequestMethod.GET)
    public String getFindReaderPage(Model model) {

        LOGGER.debug("Reader getFindReaderPage is executed!");

        model.addAttribute("foundReaders", new ArrayList<Reader>());
        model.addAttribute("reader", new Reader());

        return "findReader";
    }

    @RequestMapping(value = "/reader/find", method = RequestMethod.POST)
    public String displayFoundReaders(@ModelAttribute("reader") Reader reader, Model model) {

        LOGGER.debug("Reader displayFoudReaders is executed!");

        model.addAttribute("foundReaders", readerQueryService.findAllBySurname(reader.getSurname()));
        model.addAttribute("reader", reader);

        return "findReader";
    }

    @RequestMapping(value = "/reader/borrow/{id}", method = RequestMethod.GET)
    public String displayBorrowedBooks(@PathVariable("id") Long id,
                                       @ModelAttribute("flashbook") FoundBookDTO bookDTO, Model model) {

        LOGGER.debug("Reader displayBorrowedBooks is executed!");

        model.addAttribute("foundBooks", bookQueryService.findAllByReaderId(id));
        model.addAttribute("bookDTO", bookDTO);
        model.addAttribute("id", id);

        return "borrowBook";
    }

    @RequestMapping(value = "/reader/borrowBook/{id}", method = RequestMethod.POST)
    public String borrowBook(@Valid @ModelAttribute("bookDTO") FoundBookDTO bookDTO, BindingResult bindingResult,
                             RedirectAttributes redirectAttributes, Model model, @PathVariable("id") Long id, Locale locale) {

        LOGGER.debug("Reader borrowBook is executed!");

        if (bindingResult.hasErrors()) {
            redirectAttributes.addFlashAttribute("org.springframework.validation.BindingResult.bookDTO", bindingResult);
            redirectAttributes.addFlashAttribute("flashbook", bookDTO);

            return "redirect:/reader/borrow/" + id;
        }

        Book book;
        try {
            book = resolveBook(bookDTO);
            readerCommandService.borrow(id, book);
        } catch (BookNotFoundException ex) {
            model.addAttribute("error1", messageSource.getMessage("borrow.book.message.error1",
                    new String[]{}, locale));
            model.addAttribute("foundBooks", bookQueryService.findAllByReaderId(id));
            return "borrowBook";
        } catch (BookAlreadyBorrowedException ex) {
            model.addAttribute("error2", messageSource.getMessage("borrow.book.message.error2",
                    new String[]{}, locale));
            model.addAttribute("foundBooks", bookQueryService.findAllByReaderId(id));
            return "borrowBook";
        }


        return "redirect:/reader/borrow/" + id;
    }

    private Book resolveBook(FoundBookDTO foundBookDTO) {
        Book book;

        try {
            book = bookQueryService.findById(foundBookDTO.getBookID());
            book.setBorrowDate(foundBookDTO.getBorrowDate());
        } catch (NullPointerException ex) {
            throw new BookNotFoundException();
        }

        return book;
    }

    @RequestMapping(value = "/reader/account/{id}", method = RequestMethod.GET)
    public String displayStateOfAccount(@PathVariable("id") Long id, Model model) {

        LOGGER.debug("Reader displayStateOfAccount is executed!");

        model.addAttribute("foundBooks", bookQueryService.findAllByReaderId(id));
        model.addAttribute("id", id);

        return "returnBook";
    }

    @RequestMapping(value = "/reader/account/{id}/{bookId}", method = RequestMethod.GET)
    public String returnBook(@PathVariable("id") Long id, @PathVariable("bookId") Long bookId) {

        LOGGER.debug("Reader returnBook is executed!");

        readerCommandService.returnBook(id, bookId);

        return "redirect:/reader/account/" + id;
    }

    @ExceptionHandler(ReaderContainsBooksException.class)
    public String canotRemoveReader() {

        return "removeReaderConflict";
    }
}
