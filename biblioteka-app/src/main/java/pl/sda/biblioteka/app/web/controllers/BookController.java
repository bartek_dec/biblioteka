package pl.sda.biblioteka.app.web.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import pl.sda.biblioteka.domain.entity.Book;
import pl.sda.biblioteka.service.book.command.BookCommandService;
import pl.sda.biblioteka.service.book.exception.CannotRemoveBorrowedBookException;
import pl.sda.biblioteka.service.book.query.BookQueryService;
import pl.sda.biblioteka.service.bookCategory.exception.BookCategoryNotFoundException;
import pl.sda.biblioteka.service.bookCategory.query.BookCategoryQueryService;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.Locale;

@Controller
public class BookController {

    private static final Logger LOGGER = LoggerFactory.getLogger(BookController.class);

    private final BookQueryService bookQueryService;
    private final BookCommandService bookCommandService;
    private final BookCategoryQueryService bookCategoryQueryService;
    private final MessageSource messageSource;


    @Autowired
    public BookController(BookQueryService bookQueryService,
                          BookCommandService bookCommandService,
                          BookCategoryQueryService bookCategoryQueryService,
                          MessageSource messageSource) {
        this.bookQueryService = bookQueryService;
        this.bookCommandService = bookCommandService;
        this.bookCategoryQueryService = bookCategoryQueryService;
        this.messageSource = messageSource;
    }

    @RequestMapping(value = "/book", method = RequestMethod.GET)
    public String getPage(@ModelAttribute("flashBook") Book book, Model model) {

        LOGGER.debug("Book getPage is executed");

        model.addAttribute("listBooks", bookQueryService.findAll());
        model.addAttribute("book", book);
        model.addAttribute("categories", bookCategoryQueryService.findAll());

        return "addBook";
    }

    @RequestMapping(value = "/book/save", method = RequestMethod.POST)
    public String saveBook(@Valid @ModelAttribute("book") Book book,
                           BindingResult bindingResult, RedirectAttributes redirectAttributes,
                           Model model, Locale locale) {

        LOGGER.debug("Book saveBook is executed");

        if (bindingResult.hasErrors()) {
            redirectAttributes.addFlashAttribute("org.springframework.validation.BindingResult.book", bindingResult);
            redirectAttributes.addFlashAttribute("flashBook", book);

            return "redirect:/book";
        }

        if (book.getId() == null) {
            try {
                bookCommandService.create(book);
            } catch (BookCategoryNotFoundException ex) {
                model.addAttribute("error", messageSource.getMessage("bookCategory.message.error",
                        new String[]{}, locale));
                model.addAttribute("listBooks", bookQueryService.findAll());
                model.addAttribute("categories", bookCategoryQueryService.findAll());
                return "addBook";
            }
        } else {
            try {
                bookCommandService.update(book);
            } catch (BookCategoryNotFoundException ex) {
                model.addAttribute("error", messageSource.getMessage("bookCategory.message.error",
                        new String[]{}, locale));
                model.addAttribute("listBooks", bookQueryService.findAll());
                model.addAttribute("categories", bookCategoryQueryService.findAll());
                return "addBook";
            }
        }

        return "redirect:/book";
    }

    @RequestMapping(value = "/book/edit/{id}")
    public String editBook(@PathVariable("id") Long id, Model model) {

        LOGGER.debug("Book editBook is executed");

        model.addAttribute("listBooks", bookQueryService.findAll());
        model.addAttribute("book", bookQueryService.findById(id));
        model.addAttribute("categories", bookCategoryQueryService.findAll());

        return "addBook";
    }

    @RequestMapping(value = "/book/delete/{id}")
    public String deleteBook(@PathVariable("id") Long id) {

        LOGGER.debug("Book deleteBook is executed");

        bookCommandService.delete(id);

        return "redirect:/book";
    }

    @RequestMapping(value = "/book/find", method = RequestMethod.GET)
    public String findBookGetPage(Model model) {

        LOGGER.debug("Book findBookGetPage is executed");

        model.addAttribute("foundBooks", new ArrayList<Book>());
        model.addAttribute("book", new Book());

        return "findBook";
    }

    @RequestMapping(value = "/book/find", method = RequestMethod.POST)
    public String displayFoundBooks(@ModelAttribute("book") Book book, Model model) {

        LOGGER.debug("Book displayFoundBooks is executed");

        if (!book.getTitle().equals("") && book.getAuthorSurname().equals("")) {
            model.addAttribute("foundBooks", bookQueryService.findAllByTitle(book.getTitle()));
        } else if (book.getTitle().equals("") && !book.getAuthorSurname().equals("")) {
            model.addAttribute("foundBooks", bookQueryService.findAllByAuthorSurname(book.getAuthorSurname()));
        } else {
            model.addAttribute("foundBooks", bookQueryService.findAllByTitleAndAuthorSurname(book.getTitle(),
                    book.getAuthorSurname()));
        }

        return "findBook";
    }


    @ExceptionHandler(CannotRemoveBorrowedBookException.class)
    public String removeBookConflict() {

        LOGGER.debug("Book removeBookConflict is executed");

        return "removeBookConflict";
    }
}
