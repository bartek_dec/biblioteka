package pl.sda.biblioteka.app.configuration;

import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.filter.CharacterEncodingFilter;
import org.springframework.web.servlet.DispatcherServlet;

import javax.servlet.FilterRegistration;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration;

public class WebAppInitializer implements WebApplicationInitializer {

    @Override
    public void onStartup(javax.servlet.ServletContext servletContext) throws ServletException {
        AnnotationConfigWebApplicationContext applicationContext = getApplicationContext();

        servletContext.addListener(new ContextLoaderListener(applicationContext));

        registerCustomFilters(servletContext);
        ServletRegistration.Dynamic dispatcher = servletContext.addServlet("dispatcher",
                new DispatcherServlet(getDispatcherContext()));

        configureDispatcher(dispatcher);
    }

    private AnnotationConfigWebApplicationContext getApplicationContext() {
        AnnotationConfigWebApplicationContext context = new AnnotationConfigWebApplicationContext();
        context.register(SecurityConfiguration.class);
        context.register(AppConfiguration.class);

        return context;
    }

    private void registerCustomFilters(ServletContext servletContext) {
        FilterRegistration.Dynamic filterEncoding = servletContext.addFilter("encodingFilter",
                new CharacterEncodingFilter());
        filterEncoding.setInitParameter("encoding", "UTF-8");
        filterEncoding.setInitParameter("forceEncoding", "true");
        filterEncoding.addMappingForUrlPatterns(null, false, "/*");
    }

    private void configureDispatcher(ServletRegistration.Dynamic dispatcher) {
        dispatcher.setLoadOnStartup(1);
        dispatcher.addMapping("/");
    }

    private AnnotationConfigWebApplicationContext getDispatcherContext() {
        AnnotationConfigWebApplicationContext dispatcherContext = new AnnotationConfigWebApplicationContext();
        dispatcherContext.register(WebConfiguration.class);

        return dispatcherContext;
    }
}
