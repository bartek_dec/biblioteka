package pl.sda.biblioteka.app.web.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import pl.sda.biblioteka.domain.entity.BookCategory;
import pl.sda.biblioteka.service.bookCategory.command.BookCategoryCommandService;
import pl.sda.biblioteka.service.bookCategory.exception.BookCategoryContainsBooksException;
import pl.sda.biblioteka.service.bookCategory.query.BookCategoryQueryService;

import javax.validation.Valid;

@Controller
public class BookCategoryController {

    private static final Logger LOGGER = LoggerFactory.getLogger(BookCategoryController.class);

    private final BookCategoryCommandService bookCategoryCommandService;
    private final BookCategoryQueryService bookCategoryQueryService;

    @Autowired
    public BookCategoryController(BookCategoryCommandService bookCategoryCommandService,
                                  BookCategoryQueryService bookCategoryQueryService) {
        this.bookCategoryCommandService = bookCategoryCommandService;
        this.bookCategoryQueryService = bookCategoryQueryService;
    }

    @RequestMapping(value = "/bookCategory", method = RequestMethod.GET)
    public String getPage(@ModelAttribute("flashBookCategory") BookCategory bookCategory, Model model) {

        LOGGER.debug("Book category getPage is executed!");
        model.addAttribute("listBookCategories", bookCategoryQueryService.findAll());
        model.addAttribute("bookCategory", bookCategory);

        return "addBookCategory";
    }

    @RequestMapping(value = "/bookCategory/save", method = RequestMethod.POST)
    public String saveBookCategory(@Valid @ModelAttribute("bookCategory") BookCategory bookCategory,
                                   BindingResult bindingResult, RedirectAttributes redirectAttributes) {
        LOGGER.debug("Book category saveBookCategory is executed!");

        if (bindingResult.hasErrors()) {
            redirectAttributes.addFlashAttribute("org.springframework.validation.BindingResult.bookCategory", bindingResult);
            redirectAttributes.addFlashAttribute("flashBookCategory", bookCategory);

            return "redirect:/bookCategory";
        }

        if (bookCategory.getId() == null) {
            bookCategoryCommandService.create(bookCategory);
        } else {
            bookCategoryCommandService.update(bookCategory);
        }

        return "redirect:/bookCategory";
    }

    @RequestMapping(value = "/bookCategory/edit/{id}")
    public String editBookCategory(@PathVariable("id") Long id, Model model) {

        LOGGER.debug("Book category editBookCategory is executed!");

        model.addAttribute("listBookCategories", bookCategoryQueryService.findAll());
        model.addAttribute("bookCategory", bookCategoryQueryService.findById(id));

        return "addBookCategory";
    }

    @RequestMapping(value = "/bookCategory/delete/{id}")
    public String deleteBookCategory(@PathVariable("id") Long id) {

        LOGGER.debug("Book category deleteBookCategory is executed!");

        bookCategoryCommandService.delete(id);

        return "redirect:/bookCategory";
    }

    @ExceptionHandler(BookCategoryContainsBooksException.class)
    public String conflict() {

        return "removeBookCategoryConflict";
    }
}
