# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

This application helps to manage books in library. It offers the following functionalities:

- adding new book category to database
- removing existing book category from database
- editing existing book category

- adding new book to database
- removing existing book from database
- editing existing book

- adding new reader to database
- removing existing reader from database
- editing existing reader

- searching book in database by title, by author's surname or by both

- searching reader in database by reader's surname

- after search appropriate reader you can: borrow him books, check how many books he already has borrowed
and you can return books back to library

### How do I get set up? ###
###What you'll need:###

JDK 1.8 or later

Maven 3 or later

MySQL Server 5.7 or later

Tomcat 8.5.23 or later

###Stack:###

Spring MVC

Spring Security

Spring Data JPA

Maven

JSP

MySQL

###Before you run application do the following:###

create schema in mysql named: biblioteka

create mysql username: training

create mysql password: training


###In order to login to application use:###

login: admin

password: admin