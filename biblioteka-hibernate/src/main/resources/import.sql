INSERT INTO `biblioteka`.`BOOK_CATEGORY` (`LATEST_VERSION`, `CATEGORY`, `CATEGORY_IDENTIFIER`) VALUES ('0', 'Dział ogólny', 'DO');
INSERT INTO `biblioteka`.`BOOK_CATEGORY` (`LATEST_VERSION`, `CATEGORY`, `CATEGORY_IDENTIFIER`) VALUES ('0', 'Filozofia', 'FIL');
INSERT INTO `biblioteka`.`BOOK_CATEGORY` (`LATEST_VERSION`, `CATEGORY`, `CATEGORY_IDENTIFIER`) VALUES ('0', 'Psychologia', 'PSYCH');
INSERT INTO `biblioteka`.`BOOK_CATEGORY` (`LATEST_VERSION`, `CATEGORY`, `CATEGORY_IDENTIFIER`) VALUES ('0', 'Religioznawstwo', 'REL');
INSERT INTO `biblioteka`.`BOOK_CATEGORY` (`LATEST_VERSION`, `CATEGORY`, `CATEGORY_IDENTIFIER`) VALUES ('0', 'Socjologia', 'SOC');
INSERT INTO `biblioteka`.`BOOK_CATEGORY` (`LATEST_VERSION`, `CATEGORY`, `CATEGORY_IDENTIFIER`) VALUES ('0', 'Polityka', 'POL');
INSERT INTO `biblioteka`.`BOOK_CATEGORY` (`LATEST_VERSION`, `CATEGORY`, `CATEGORY_IDENTIFIER`) VALUES ('0', 'Ekonomia', 'EKM');
INSERT INTO `biblioteka`.`BOOK_CATEGORY` (`LATEST_VERSION`, `CATEGORY`, `CATEGORY_IDENTIFIER`) VALUES ('0', 'Prawo', 'PR');
INSERT INTO `biblioteka`.`BOOK_CATEGORY` (`LATEST_VERSION`, `CATEGORY`, `CATEGORY_IDENTIFIER`) VALUES ('0', 'Medycyna', 'MD');

INSERT INTO `biblioteka`.`READER` (`LATEST_VERSION`, `CITY`, `FLAT_NUMBER`, `HOUSE_NUMBER`, `POSTAL_CODE`, `STREET`, `BIRTHDATE`, `EMAIL`, `NAME`, `SURNAME`) VALUES ('0', 'Kamionka', '', '2', '21-132', 'Wesoła', '1987-03-14', 'decbartek@gmail.com', 'Bartek', 'Dec');
INSERT INTO `biblioteka`.`READER` (`LATEST_VERSION`, `CITY`, `FLAT_NUMBER`, `HOUSE_NUMBER`, `POSTAL_CODE`, `STREET`, `BIRTHDATE`, `EMAIL`, `NAME`, `SURNAME`) VALUES ('0', 'Lublin', '', '3', '20-100', 'Zana', '1989-04-15', 'jakubj@gmail.com', 'Jakub', 'Jarocha');
INSERT INTO `biblioteka`.`READER` (`LATEST_VERSION`, `CITY`, `FLAT_NUMBER`, `HOUSE_NUMBER`, `POSTAL_CODE`, `STREET`, `BIRTHDATE`, `EMAIL`, `NAME`, `SURNAME`) VALUES ('0', 'Lublin', '', '4', '22-112', 'Balladyny', '1993-05-16', 'adrianszk@gmail.com', 'Adrian', 'Szczepański');
INSERT INTO `biblioteka`.`READER` (`LATEST_VERSION`, `CITY`, `FLAT_NUMBER`, `HOUSE_NUMBER`, `POSTAL_CODE`, `STREET`, `BIRTHDATE`, `EMAIL`, `NAME`, `SURNAME`) VALUES ('0', 'Lublin', '', '6', '23-114', 'Wajdeloty', '1989-06-17', 'kamilgk@gmail.com', 'Kamil', 'Gocłowski');
INSERT INTO `biblioteka`.`READER` (`LATEST_VERSION`, `CITY`, `FLAT_NUMBER`, `HOUSE_NUMBER`, `POSTAL_CODE`, `STREET`, `BIRTHDATE`, `EMAIL`, `NAME`, `SURNAME`) VALUES ('0', 'Lublin', '', '7', '22-115', 'Wajdeloty', '1988-04-16', 'karolb@gmail.com', 'Karol', 'Barczuk');
INSERT INTO `biblioteka`.`READER` (`LATEST_VERSION`, `CITY`, `FLAT_NUMBER`, `HOUSE_NUMBER`, `POSTAL_CODE`, `STREET`, `BIRTHDATE`, `EMAIL`, `NAME`, `SURNAME`) VALUES ('0', 'Świdnik', '', '8', '21-116', 'Wileńska', '1989-06-18', 'katarzynaa@gmail.com', 'Katarzyna', 'Adamczyk');
INSERT INTO `biblioteka`.`READER` (`LATEST_VERSION`, `CITY`, `FLAT_NUMBER`, `HOUSE_NUMBER`, `POSTAL_CODE`, `STREET`, `BIRTHDATE`, `EMAIL`, `NAME`, `SURNAME`) VALUES ('0', 'Lublin', '', '12', '24-100', 'Juranda', '1990-10-17', 'lukaszs@gmail.com', 'Łukasz', 'Sajnóg');
INSERT INTO `biblioteka`.`READER` (`LATEST_VERSION`, `CITY`, `FLAT_NUMBER`, `HOUSE_NUMBER`, `POSTAL_CODE`, `STREET`, `BIRTHDATE`, `EMAIL`, `NAME`, `SURNAME`) VALUES ('0', 'Lublin', '', '22', '24-120', 'Żarnowiecka', '1990-11-13', 'arkadiuszr@gmail.com', 'Arkadiusz', 'Rybak');
INSERT INTO `biblioteka`.`READER` (`LATEST_VERSION`, `CITY`, `FLAT_NUMBER`, `HOUSE_NUMBER`, `POSTAL_CODE`, `STREET`, `BIRTHDATE`, `EMAIL`, `NAME`, `SURNAME`) VALUES ('0', 'Lubartów', '', '13', '21-122', 'Jana Sawy', '1988-01-15', 'pawelb@gmail.com', 'Paweł', 'Burzak');
INSERT INTO `biblioteka`.`READER` (`LATEST_VERSION`, `CITY`, `FLAT_NUMBER`, `HOUSE_NUMBER`, `POSTAL_CODE`, `STREET`, `BIRTHDATE`, `EMAIL`, `NAME`, `SURNAME`) VALUES ('0', 'Lublin', '', '15', '22-131', 'Filaretów', '1986-02-21', 'kamilak@gmail.com', 'Kamila', 'Koszowska');
INSERT INTO `biblioteka`.`READER` (`LATEST_VERSION`, `CITY`, `FLAT_NUMBER`, `HOUSE_NUMBER`, `POSTAL_CODE`, `STREET`, `BIRTHDATE`, `EMAIL`, `NAME`, `SURNAME`) VALUES ('0', 'Lublin', '', '27', '21-133', 'Głęboka', '1993-08-23', 'pawelj@gmail.com', 'Paweł', 'Jezior');
INSERT INTO `biblioteka`.`READER` (`LATEST_VERSION`, `CITY`, `FLAT_NUMBER`, `HOUSE_NUMBER`, `POSTAL_CODE`, `STREET`, `BIRTHDATE`, `EMAIL`, `NAME`, `SURNAME`) VALUES ('0', 'Lublin', '', '28', '23-124', 'Weteranów', '1989-09-17', 'tomaszr@gmail.com', 'Tomasz', 'Rojek');
INSERT INTO `biblioteka`.`READER` (`LATEST_VERSION`, `CITY`, `FLAT_NUMBER`, `HOUSE_NUMBER`, `POSTAL_CODE`, `STREET`, `BIRTHDATE`, `EMAIL`, `NAME`, `SURNAME`) VALUES ('0', 'Świdnik', '', '32', '21-132', 'Puławska', '1988-012-03', 'marcink@gmail.com', 'Marcin', 'Korniszuk');


INSERT INTO `biblioteka`.`BOOK` (`LATEST_VERSION`, `AUTHOR_FIRST_NAME`, `AUTHOR_SURNAME`, `TITLE`, `category_ID`) VALUES ('0', 'T.', 'Doreste', 'Astronauta z Palengue', '1');
INSERT INTO `biblioteka`.`BOOK` (`LATEST_VERSION`, `AUTHOR_FIRST_NAME`, `AUTHOR_SURNAME`, `TITLE`, `category_ID`) VALUES ('0', 'T.', 'Doreste', 'Astronauta z Palengue', '1');
INSERT INTO `biblioteka`.`BOOK` (`LATEST_VERSION`, `AUTHOR_FIRST_NAME`, `AUTHOR_SURNAME`, `TITLE`, `category_ID`) VALUES ('0', 'E.V.', 'Daniken', 'Z powrotem do gwiazd', '1');
INSERT INTO `biblioteka`.`BOOK` (`LATEST_VERSION`, `AUTHOR_FIRST_NAME`, `AUTHOR_SURNAME`, `TITLE`, `category_ID`) VALUES ('0', 'M.', 'Łukasiewicz', 'Bez wyjaśnienia', '1');
INSERT INTO `biblioteka`.`BOOK` (`LATEST_VERSION`, `AUTHOR_FIRST_NAME`, `AUTHOR_SURNAME`, `TITLE`, `category_ID`) VALUES ('0', 'M.', 'Łukasiewicz', 'Bez wyjaśnienia', '1');

INSERT INTO `biblioteka`.`BOOK` (`LATEST_VERSION`, `AUTHOR_FIRST_NAME`, `AUTHOR_SURNAME`, `TITLE`, `category_ID`) VALUES ('0', 'M.', 'Szyszkowska', 'Za horyzontem', '2');
INSERT INTO `biblioteka`.`BOOK` (`LATEST_VERSION`, `AUTHOR_FIRST_NAME`, `AUTHOR_SURNAME`, `TITLE`, `category_ID`) VALUES ('0', 'K.', 'Konarzewska', 'Astrologia', '2');
INSERT INTO `biblioteka`.`BOOK` (`LATEST_VERSION`, `AUTHOR_FIRST_NAME`, `AUTHOR_SURNAME`, `TITLE`, `category_ID`) VALUES ('0', 'K.', 'Konarzewska', 'Astrologia', '2');
INSERT INTO `biblioteka`.`BOOK` (`LATEST_VERSION`, `AUTHOR_FIRST_NAME`, `AUTHOR_SURNAME`, `TITLE`, `category_ID`) VALUES ('0', 'T.', 'Dethlefsen', 'Sukces życia', '2');
INSERT INTO `biblioteka`.`BOOK` (`LATEST_VERSION`, `AUTHOR_FIRST_NAME`, `AUTHOR_SURNAME`, `TITLE`, `category_ID`) VALUES ('0', 'W.', 'Tatarkiewicz', 'O szczęściu', '2');

INSERT INTO `biblioteka`.`BOOK` (`LATEST_VERSION`, `AUTHOR_FIRST_NAME`, `AUTHOR_SURNAME`, `TITLE`, `category_ID`) VALUES ('0', 'M.G.', 'Jaroszewski', 'Psychologia XX wieku', '3');
INSERT INTO `biblioteka`.`BOOK` (`LATEST_VERSION`, `AUTHOR_FIRST_NAME`, `AUTHOR_SURNAME`, `TITLE`, `category_ID`) VALUES ('0', 'J.', 'Santorski', 'Emocje, charaktery i geny', '3');
INSERT INTO `biblioteka`.`BOOK` (`LATEST_VERSION`, `AUTHOR_FIRST_NAME`, `AUTHOR_SURNAME`, `TITLE`, `category_ID`) VALUES ('0', 'L.', 'Jampolsky', 'Leczenia uzależnionego umysłu', '3');
INSERT INTO `biblioteka`.`BOOK` (`LATEST_VERSION`, `AUTHOR_FIRST_NAME`, `AUTHOR_SURNAME`, `TITLE`, `category_ID`) VALUES ('0', 'A.', 'Mortczork', 'Zarys psychologii rozwoju', '3');
INSERT INTO `biblioteka`.`BOOK` (`LATEST_VERSION`, `AUTHOR_FIRST_NAME`, `AUTHOR_SURNAME`, `TITLE`, `category_ID`) VALUES ('0', 'J.', 'Trent', 'Błędne koło rozwodów', '3');

INSERT INTO `biblioteka`.`BOOK` (`LATEST_VERSION`, `AUTHOR_FIRST_NAME`, `AUTHOR_SURNAME`, `TITLE`, `category_ID`) VALUES ('0', 'P.', 'Słabek', 'Święty Mikołaj', '4');
INSERT INTO `biblioteka`.`BOOK` (`LATEST_VERSION`, `AUTHOR_FIRST_NAME`, `AUTHOR_SURNAME`, `TITLE`, `category_ID`) VALUES ('0', 'P.', 'Słabek', 'Pięć wielkich religii świata', '4');
INSERT INTO `biblioteka`.`BOOK` (`LATEST_VERSION`, `AUTHOR_FIRST_NAME`, `AUTHOR_SURNAME`, `TITLE`, `category_ID`) VALUES ('0', 'A.', 'Świderkówna', 'Rozmowy o Biblii', '4');
INSERT INTO `biblioteka`.`BOOK` (`LATEST_VERSION`, `AUTHOR_FIRST_NAME`, `AUTHOR_SURNAME`, `TITLE`, `category_ID`) VALUES ('0', 'C.S.', 'Lewis', 'Listy starego diabła do młodego', '4');
INSERT INTO `biblioteka`.`BOOK` (`LATEST_VERSION`, `AUTHOR_FIRST_NAME`, `AUTHOR_SURNAME`, `TITLE`, `category_ID`) VALUES ('0', 'P.', 'Dzedzej', 'Porzucone sutanny', '4');

INSERT INTO `biblioteka`.`BOOK` (`LATEST_VERSION`, `AUTHOR_FIRST_NAME`, `AUTHOR_SURNAME`, `TITLE`, `category_ID`) VALUES ('0', 'G.', 'Gilbert', 'Krzyk młodych', '5');
INSERT INTO `biblioteka`.`BOOK` (`LATEST_VERSION`, `AUTHOR_FIRST_NAME`, `AUTHOR_SURNAME`, `TITLE`, `category_ID`) VALUES ('0', 'W.', 'Kulesza', 'Ideologie naszych czasów', '5');
INSERT INTO `biblioteka`.`BOOK` (`LATEST_VERSION`, `AUTHOR_FIRST_NAME`, `AUTHOR_SURNAME`, `TITLE`, `category_ID`) VALUES ('0', 'J.', 'Scott', 'Lekcje madame Chic', '5');
INSERT INTO `biblioteka`.`BOOK` (`LATEST_VERSION`, `AUTHOR_FIRST_NAME`, `AUTHOR_SURNAME`, `TITLE`, `category_ID`) VALUES ('0', 'M.', 'Jędrzejko', 'Dzieci a multimedia', '5');
INSERT INTO `biblioteka`.`BOOK` (`LATEST_VERSION`, `AUTHOR_FIRST_NAME`, `AUTHOR_SURNAME`, `TITLE`, `category_ID`) VALUES ('0', 'J.', 'Kirenko', 'Oblicza niepełnosprawności', '5');

INSERT INTO `biblioteka`.`BOOK` (`LATEST_VERSION`, `AUTHOR_FIRST_NAME`, `AUTHOR_SURNAME`, `TITLE`, `category_ID`) VALUES ('0', 'S.', 'Tansey', 'Nauki polityczne', '6');
INSERT INTO `biblioteka`.`BOOK` (`LATEST_VERSION`, `AUTHOR_FIRST_NAME`, `AUTHOR_SURNAME`, `TITLE`, `category_ID`) VALUES ('0', 'R.', 'Kapuściński', 'Cesarz', '6');
INSERT INTO `biblioteka`.`BOOK` (`LATEST_VERSION`, `AUTHOR_FIRST_NAME`, `AUTHOR_SURNAME`, `TITLE`, `category_ID`) VALUES ('0', 'C.', 'Gearty', 'Terroryzm', '6');
INSERT INTO `biblioteka`.`BOOK` (`LATEST_VERSION`, `AUTHOR_FIRST_NAME`, `AUTHOR_SURNAME`, `TITLE`, `category_ID`) VALUES ('0', 'R.', 'Kapuściński', 'Chrystus z karabinem na ramieniu', '6');
INSERT INTO `biblioteka`.`BOOK` (`LATEST_VERSION`, `AUTHOR_FIRST_NAME`, `AUTHOR_SURNAME`, `TITLE`, `category_ID`) VALUES ('0', 'T.', 'Lis', 'Co z tą Polską', '6');

INSERT INTO `biblioteka`.`BOOK` (`LATEST_VERSION`, `AUTHOR_FIRST_NAME`, `AUTHOR_SURNAME`, `TITLE`, `category_ID`) VALUES ('0', 'R.T.', 'Kiyosaki', 'Bogaty ojciec idealny ojciec', '7');
INSERT INTO `biblioteka`.`BOOK` (`LATEST_VERSION`, `AUTHOR_FIRST_NAME`, `AUTHOR_SURNAME`, `TITLE`, `category_ID`) VALUES ('0', 'R.T.', 'Kiyosaki', 'Bogaty ojciec idealny ojciec', '7');
INSERT INTO `biblioteka`.`BOOK` (`LATEST_VERSION`, `AUTHOR_FIRST_NAME`, `AUTHOR_SURNAME`, `TITLE`, `category_ID`) VALUES ('0', 'J.', 'Bański', 'Geografia polskiej wsi', '7');
INSERT INTO `biblioteka`.`BOOK` (`LATEST_VERSION`, `AUTHOR_FIRST_NAME`, `AUTHOR_SURNAME`, `TITLE`, `category_ID`) VALUES ('0', 'J.', 'Bański', 'Geografia polskiej wsi', '7');
INSERT INTO `biblioteka`.`BOOK` (`LATEST_VERSION`, `AUTHOR_FIRST_NAME`, `AUTHOR_SURNAME`, `TITLE`, `category_ID`) VALUES ('0', 'B.', 'Ehreureich', 'Za grosze pracować i (nie) przeżyć', '7');

INSERT INTO `biblioteka`.`BOOK` (`LATEST_VERSION`, `AUTHOR_FIRST_NAME`, `AUTHOR_SURNAME`, `TITLE`, `category_ID`) VALUES ('0', 'B.', 'Howerka', 'Prawo autorskie w pracy bibliotekarza', '8');
INSERT INTO `biblioteka`.`BOOK` (`LATEST_VERSION`, `AUTHOR_FIRST_NAME`, `AUTHOR_SURNAME`, `TITLE`, `category_ID`) VALUES ('0', 'A.', 'Filipowicz', 'Prawo dla ekonomistów', '8');
INSERT INTO `biblioteka`.`BOOK` (`LATEST_VERSION`, `AUTHOR_FIRST_NAME`, `AUTHOR_SURNAME`, `TITLE`, `category_ID`) VALUES ('0', 'A.', 'Filipowicz', 'Prawo dla ekonomistów', '8');
INSERT INTO `biblioteka`.`BOOK` (`LATEST_VERSION`, `AUTHOR_FIRST_NAME`, `AUTHOR_SURNAME`, `TITLE`, `category_ID`) VALUES ('0', 'W.', 'Siuda', 'Elementy prawa dla ekonomistów', '8');
INSERT INTO `biblioteka`.`BOOK` (`LATEST_VERSION`, `AUTHOR_FIRST_NAME`, `AUTHOR_SURNAME`, `TITLE`, `category_ID`) VALUES ('0', 'W.', 'Siuda', 'Elementy prawa dla ekonomistów', '8');

INSERT INTO `biblioteka`.`BOOK` (`LATEST_VERSION`, `AUTHOR_FIRST_NAME`, `AUTHOR_SURNAME`, `TITLE`, `category_ID`) VALUES ('0', 'K.T.', 'Teoplitz', 'Tytoniowy szlak', '9');
INSERT INTO `biblioteka`.`BOOK` (`LATEST_VERSION`, `AUTHOR_FIRST_NAME`, `AUTHOR_SURNAME`, `TITLE`, `category_ID`) VALUES ('0', 'B.', 'Niggemann', 'Alergia u dzieci', '9');
INSERT INTO `biblioteka`.`BOOK` (`LATEST_VERSION`, `AUTHOR_FIRST_NAME`, `AUTHOR_SURNAME`, `TITLE`, `category_ID`) VALUES ('0', 'R.', 'Aleksandrowicz', 'Mały atlas anatomiczny', '9');
INSERT INTO `biblioteka`.`BOOK` (`LATEST_VERSION`, `AUTHOR_FIRST_NAME`, `AUTHOR_SURNAME`, `TITLE`, `category_ID`) VALUES ('0', 'S.', 'Torkos', 'Indeks glikemiczny', '9');
INSERT INTO `biblioteka`.`BOOK` (`LATEST_VERSION`, `AUTHOR_FIRST_NAME`, `AUTHOR_SURNAME`, `TITLE`, `category_ID`) VALUES ('0', 'D.', 'Podlech', 'Rośliny lecznicze', '9');

