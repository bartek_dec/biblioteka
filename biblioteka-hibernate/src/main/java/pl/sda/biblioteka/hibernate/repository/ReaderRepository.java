package pl.sda.biblioteka.hibernate.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import pl.sda.biblioteka.domain.entity.Reader;

import java.util.List;

@Repository
public interface ReaderRepository extends JpaRepository<Reader, Long> {

    @Query(value = "SELECT a FROM Reader a WHERE LOWER(a.surname) = LOWER(:surname)")
    List<Reader> findAllBySurname(@Param("surname") String surname);

}