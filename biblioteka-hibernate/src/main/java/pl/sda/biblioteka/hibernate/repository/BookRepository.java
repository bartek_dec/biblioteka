package pl.sda.biblioteka.hibernate.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import pl.sda.biblioteka.domain.entity.Book;

import java.util.List;

@Repository
public interface BookRepository extends JpaRepository<Book, Long> {

    @Query(value = "SELECT a FROM Book a WHERE LOWER(a.title)=LOWER(:title) ")
    List<Book> findAllByTitle(@Param("title") String title);

    @Query(value = "SELECT b FROM Book b WHERE LOWER(b.authorSurname)=LOWER(:surname) ")
    List<Book> findAllByAuthorSurname(@Param("surname") String surname);

    @Query(value = "SELECT a FROM Book a WHERE LOWER(a.title)=LOWER(:title) and LOWER(a.authorSurname)=LOWER(:surname) ")
    List<Book> findAllByTitleAndAuthorSurname(@Param("title") String title, @Param("surname") String surname);

    @Query(value = "SELECT a FROM Book a WHERE a.reader.id=:id")
    List<Book> findAllByReaderId(@Param("id") Long id);
}
